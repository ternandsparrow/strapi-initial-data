'use strict'
const strapiInitialData = require('strapi-initial-data')

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/concepts/configurations.html#bootstrap
 */

module.exports = () => {
  // if you don't need data load to block the server start, don't await it
  // you must pass the `strapi` global to the function
  strapiInitialData(strapi).catch(err => {
    // using console over strapi.log because strapi doesn't handle errors well
    console.error('Failed to load all initial data', err)
  })
}
