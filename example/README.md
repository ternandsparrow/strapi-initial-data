# Example application

This shows an example of how to use `strapi-initial-data`.

See the [animal-type](./api/animal-type/models/animal-type.settings.json) model
definition for a full example.

# Quickstart example

 1. clone this repo
 1. install the dependencies for the example:
    ```bash
    yarn install
    ```
 1. start the server:
    ```bash
    yarn develop
    ```
 1. note that data was loaded (`created=2`) for our model as none existed:
    ```
    ...
    [2021-02-02T04:30:21.003Z] info Initial data for animal-type: created=2, updated=0, total=2
    [2021-02-02T04:30:21.004Z] info Took 53ms to load all initial data
    ...
    ```
 1. stop the server and start it again and you'll see no changes were made as
    the records already existed.
    ```
    [2021-02-02T04:40:53.318Z] info Initial data for animal-type: created=0, updated=0, total=2
    [2021-02-02T04:40:53.319Z] info Took 33ms to load all initial data
    ```
 1. hit the API endpoint to see the data:
    ```
    $ curl localhost:1337/animal-types
    [
      {
        "Name": "Mammals",
        "code": "MAM",
        "created_at": "2021-02-02T04:30:20.982Z",
        "id": 1,
        "updated_at": "2021-02-02T04:30:20.982Z"
      },
      {
        "Name": "Birds",
        "code": "BRD",
        "created_at": "2021-02-02T04:30:20.994Z",
        "id": 2,
        "updated_at": "2021-02-02T04:30:20.994Z"
      }
    ]
    ```
 1. if you add a new record, you'll see only that one will be added.

# Development
If you update the code in the parent, you need to update the lockfile for this
project with:

```bash
yarn add ../
```
